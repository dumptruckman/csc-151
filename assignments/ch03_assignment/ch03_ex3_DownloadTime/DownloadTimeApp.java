import java.util.Scanner;

/**
 * @author Jeremy Wood
 */
public class DownloadTimeApp {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        System.out.println("Welcome to the Download Time Estimator");

        do {
            System.out.println();
            System.out.print("Enter file size (MB): ");
            double fileSize = getDouble(reader);
            System.out.print("Enter download speed (MB/sec): ");
            double downloadSpeed = getDouble(reader);

            System.out.println();
            System.out.println("This download will take approximately " + getHumanReadableTime((long) (fileSize / downloadSpeed)));

            System.out.println();
            System.out.print("Continue? (y/n): ");
        } while (reader.next().equalsIgnoreCase("y"));
    }

    private static double getDouble(Scanner reader) {
        while (!reader.hasNextDouble()) {
            reader.next();
            System.out.print("That is not a valid entry. Please enter a number: ");
        }
        return reader.nextDouble();
    }

    public static String getHumanReadableTime(long second) {
        if (second == 0) {
            return "0 seconds";
        }
        long minute = second / 60;
        second = second % 60;
        long hour = minute / 60;
        minute = minute % 60;
        long day = hour / 24;
        hour = hour % 24;
        StringBuilder time = new StringBuilder();
        if (day != 0) {
            time.append(day);
        }
        if (day == 1) {
            time.append(" day ");
        } else if (day > 1) {
            time.append(" days ");
        }
        if (hour != 0) {
            time.append(hour);
        }
        if (hour == 1) {
            time.append(" hour ");
        } else if (hour > 1) {
            time.append(" hours ");
        }
        if (minute != 0) {
            time.append(minute);
        }
        if (minute == 1) {
            time.append(" minute ");
        } else if (minute > 1) {
            time.append(" minutes ");
        }
        if (second!= 0) {
            time.append(second);
        }
        if (second == 1) {
            time.append(" second");
        } else if (second > 1) {
            time.append(" seconds");
        }
        return time.toString().trim();
    }
}
