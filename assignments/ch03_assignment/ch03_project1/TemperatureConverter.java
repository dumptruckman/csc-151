import java.text.DecimalFormat;
import java.util.Scanner;

/**
 * @author Jeremy Wood
 */
public class TemperatureConverter {

    private static final DecimalFormat MAX_2_DECIMALS = new DecimalFormat("#.##");

    public static void main(String[] args) {

        Scanner reader = new Scanner(System.in);

        System.out.println("Welcome to the Temperature Converter");

        do {
            System.out.println();
            System.out.print("Enter degrees in Fahrenheit: ");
            double tempF = reader.nextDouble();
            System.out.println("Degrees in Celsius: " +  MAX_2_DECIMALS.format(convertFarenheitToCelsius(tempF)));
            System.out.println();
            System.out.print("Continue? (y/n): ");
        } while (reader.next().equalsIgnoreCase("y"));
    }

    public static double convertFarenheitToCelsius(double tempF) {
        return (tempF - 32) * 5 / 9;
    }
}
