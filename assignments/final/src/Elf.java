import java.util.Scanner;

/**
 * @author Jeremy Wood
 */
public class Elf extends Character {

    private static final int ARROW_COST = 5;
    private static final int ARROW_STACK_SIZE = 20;

    private int arrows = 0;

    public Elf(String name) {
        super(name);
    }

    public int getArrows() {
        return arrows;
    }

    public void setArrows(int arrows) {
        this.arrows = arrows;
    }

    @Override
    public void purchaseSpecial(Scanner sc) {
        int purchaseAmount = Validator.getPurchase(sc, "Arrows (x" + ARROW_STACK_SIZE + ") - " + ARROW_COST + " gold: ", ARROW_COST, getGold());
        int newAmount = ARROW_STACK_SIZE * purchaseAmount;
        arrows += newAmount;
        int cost = purchaseAmount * ARROW_COST;
        setGold(getGold() - cost);
        System.out.println("You purchased " + newAmount + " arrows for " + cost + " gold!");
    }

    @Override
    public String toString() {
        return "" + getName() + " the Elf\n"
                + "  Health: " + getHealth() + "\n"
                + "  Gold: " + getGold() + "\n"
                + "  Arrows: " + getArrows();
    }
}
