import java.util.Scanner;

/**
 * @author Jeremy Wood
 */
public class Validator {

    public static String getLine(Scanner sc, String prompt) {
        System.out.print(prompt);
        String s = sc.nextLine();        // read the whole line
        return s;
    }

    public static String getString(Scanner sc, String prompt) {
        System.out.print(prompt);
        String s = sc.next();        // read the first string on the line
        sc.nextLine();               // discard the rest of the line
        return s;
    }

    public static String getString(Scanner sc, String prompt, String... choices) {
        while (true) {
            System.out.print(prompt);
            String s = sc.next();
            sc.nextLine();
            if (choices.length == 0) {
                return s;
            }
            for (String choice : choices) {
                if (s.equalsIgnoreCase(choice)) {
                    return choice;
                }
            }
            System.out.println("Invalid choice! Try again.");
        }
    }

    public static int getInt(Scanner sc, String prompt) {
        boolean isValid = false;
        int i = 0;
        while (isValid == false) {
            System.out.print(prompt);
            if (sc.hasNextInt()) {
                i = sc.nextInt();
                isValid = true;
            } else {
                System.out.println("Error! Invalid integer value. Try again.");
            }
            sc.nextLine();  // discard any other data entered on the line
        }
        return i;
    }

    public static int getInt(Scanner sc, String prompt,
                             int min, int max) {
        int i = 0;
        boolean isValid = false;
        while (isValid == false) {
            i = getInt(sc, prompt);
            if (i <= min)
                System.out.println(
                        "Error! Number must be greater than " + min);
            else if (i >= max)
                System.out.println(
                        "Error! Number must be less than " + max);
            else
                isValid = true;
        }
        return i;
    }

    public static double getDouble(Scanner sc, String prompt) {
        boolean isValid = false;
        double d = 0;
        while (isValid == false) {
            System.out.print(prompt);
            if (sc.hasNextDouble()) {
                d = sc.nextDouble();
                isValid = true;
            } else {
                System.out.println("Error! Invalid decimal value. Try again.");
            }
            sc.nextLine();  // discard any other data entered on the line
        }
        return d;
    }

    public static double getDouble(Scanner sc, String prompt,
                                   double min, double max) {
        double d = 0;
        boolean isValid = false;
        while (isValid == false) {
            d = getDouble(sc, prompt);
            if (d <= min)
                System.out.println(
                        "Error! Number must be greater than " + min);
            else if (d >= max)
                System.out.println(
                        "Error! Number must be less than " + max);
            else
                isValid = true;
        }
        return d;
    }

    public static int getPurchase(Scanner sc, String prompt, int costPer, int remainingGold) {
        while (true) {
            int purchaseAmount = getInt(sc, prompt, -1, Integer.MAX_VALUE);
            int cost = costPer * purchaseAmount;
            if (cost > remainingGold) {
                System.out.println("Buying " + purchaseAmount + " of those would cost " + cost + " which is " + (cost - remainingGold) + " gold more than you have. Try another amount.");
                System.out.println();
            } else {
                return purchaseAmount;
            }
        }
    }
}