import java.util.Scanner;

/**
 * @author Jeremy Wood
 */
public abstract class Character {

    private String name;
    private int gold, health;

    public Character(String name) {
        this(name, 25, (int) (Math.random() * 10D) + 1);
    }

    public Character(String name, int gold, int health) {
        this.name = name;
        this.gold = gold;
        this.health = health;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public abstract void purchaseSpecial(Scanner sc);

    @Override
    public String toString() {
        return "Character{" +
                "name='" + name + '\'' +
                ", gold=" + gold +
                ", health=" + health +
                '}';
    }
}
