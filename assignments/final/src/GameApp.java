import java.util.Scanner;

/**
 * @author Jeremy Wood
 */
public class GameApp {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Welcome to the Elf/Wizard game where all you can play as is a pointed-eared humanoid or a spell casting maniac!");
        System.out.println();

        String characterType = Validator.getString(in, "What character would you like to play? (Elf/Wizard): ", "Elf", "Wizard");
        String name = Validator.getString(in, "What shall you call your new " + characterType + "? ");
        Character character = getCharacter(characterType, name);

        System.out.println();
        System.out.println("Would you like to purchase anything? (Enter 0 for none)");
        System.out.println("=======================================================");
        System.out.println("You have " + character.getGold() + " gold");
        System.out.println();
        character.purchaseSpecial(in);
        System.out.println();

        System.out.println("Your Character");
        System.out.println("==============");
        System.out.println(character.toString());
    }

    private static Character getCharacter(String characterType, String name) {
        switch (characterType) {
            case "Elf":
                return new Elf(name);
            case "Wizard":
                return new Wizard(name);
            default:
                return new Wizard(name);
        }
    }
}
