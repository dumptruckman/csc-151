import java.util.Scanner;

/**
 * @author Jeremy Wood
 */
public class Wizard extends Character {

    private static final int SPELL_COST = 5;

    private int spells = 0;

    public Wizard(String name) {
        super(name);
    }

    public int getSpells() {
        return spells;
    }

    public void setSpells(int spells) {
        this.spells = spells;
    }

    @Override
    public void purchaseSpecial(Scanner sc) {
        int purchaseAmount = Validator.getPurchase(sc, "Spell - " + SPELL_COST + " gold: ", SPELL_COST, getGold());
        spells += purchaseAmount;
        int cost = purchaseAmount * SPELL_COST;
        setGold(getGold() - cost);
        System.out.println("You purchased " + purchaseAmount + " spells for " + cost + " gold!");
    }

    @Override
    public String toString() {
        return "" + getName() + " the Wizard\n"
                + "  Health: " + getHealth() + "\n"
                + "  Gold: " + getGold() + "\n"
                + "  Spells: " + getSpells();
    }
}
