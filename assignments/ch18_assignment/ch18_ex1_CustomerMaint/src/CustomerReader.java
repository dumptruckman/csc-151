import java.util.ArrayList;

/**
 * @author Jeremy Wood
 */
public interface CustomerReader {

    Customer getCustomer(String email);

    ArrayList<Customer> getCustomers();
}