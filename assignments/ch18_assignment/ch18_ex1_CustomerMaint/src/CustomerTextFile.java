import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * @author Jeremy Wood
 */
public final class CustomerTextFile implements CustomerDAO {

    private ArrayList<Customer> customers = null;
    private Path customersPath = null;
    private File customersFile = null;

    private final String FIELD_SEP = "\t";

    public CustomerTextFile() {
        // initialize the class variables
        customersPath = Paths.get(CustomerMaintApp.USER_FILE_DIRECTORY + "customers.txt");
        customersFile = customersPath.toFile();
        customers = this.getCustomers();
    }

    public ArrayList<Customer> getCustomers() {
        // if the customers file has already been read, don't read it again
        if (customers != null)
            return customers;

        customers = new ArrayList<>();

        // load the array list with Customer objects created from
        // the data in the file
        if (Files.exists(customersPath)) {
            try (BufferedReader in = new BufferedReader(new FileReader(customersFile))) {
                String line;
                while ((line = in.readLine()) != null) {
                    String[] columns = line.split(FIELD_SEP);
                    Customer customer = new Customer(columns[0], columns[1], columns[2]);
                    customers.add(customer);
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        return customers;
    }

    public Customer getCustomer(String email) {
        for (Customer c : customers) {
            if (c.getEmail().equals(email))
                return c;
        }
        return null;
    }

    public boolean addCustomer(Customer c) {
        customers.add(c);
        return this.saveCustomers();
    }

    public boolean deleteCustomer(Customer c) {
        customers.remove(c);
        return this.saveCustomers();
    }

    public boolean updateCustomer(Customer newCustomer) {
        // get the old customer and remove it
        Customer oldCustomer = this.getCustomer(newCustomer.getEmail());
        int i = customers.indexOf(oldCustomer);
        customers.remove(i);

        // add the updated customer
        customers.add(i, newCustomer);

        return this.saveCustomers();
    }

    private boolean saveCustomers() {
        // save the Customer objects in the array list to the file
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(customersFile)))) {
            for (Customer customer : customers) {
                out.print(customer.getFirstName() + FIELD_SEP);
                out.print(customer.getLastName() + FIELD_SEP);
                out.println(customer.getEmail() + FIELD_SEP);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}