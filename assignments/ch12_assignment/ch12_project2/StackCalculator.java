import java.util.LinkedList;

/**
 * @author Jeremy
 */
public class StackCalculator {

    private final LinkedList<Double> stack = new LinkedList<>();

    public void push(double value) {
        stack.push(value);
    }

    public double pop() {
        return stack.pop();
    }

    public double[] getValues() {
        double[] values =  new double[stack.size()];
        int i = 0;
        for (Double d : stack) {
            values[i] = d;
            i++;
        }
        return values;
    }

    public int size() {
        return stack.size();
    }

    public void clear() {
        stack.clear();
    }

    public double add() {
        double d1 = pop(), d2 = pop(), res = d2 + d1;
        push(res);
        return res;
    }

    public double subtract() {
        double d1 = pop(), d2 = pop(), res = d2 - d1;
        push(res);
        return res;
    }

    public double multiply() {
        double d1 = pop(), d2 = pop(), res = d2 * d1;
        push(res);
        return res;
    }

    public double divide() {
        double d1 = pop(), d2 = pop(), res = d2 / d1;
        push(res);
        return res;
    }

    public double power() {
        double d1 = pop(), d2 = pop(), res = Math.pow(d2, d1);
        push(res);
        return res;
    }

    public double sqrt() {
        double d1 = pop(), res = Math.sqrt(d1);
        push(res);
        return res;
    }
}
