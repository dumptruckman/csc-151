import java.util.Scanner;

/**
 * @author Jeremy
 */
public class CalculatorApp {

    public static void main(String[] args) {
        System.out.println("Welcome to the Stack Calculator.");
        System.out.println();
        System.out.println("Commands: push n, add, sub, mult, div, pow, sqrt, clear, or quit");
        System.out.println("Shortcuts: n, +, -, *, /, ^");
        System.out.println();

        StackCalculator calc = new StackCalculator();
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.print("? ");

            if (sc.hasNextDouble()) {
                calc.push(sc.nextDouble());
            } else {
                String input = sc.next();
                if (input.equalsIgnoreCase("push")) {
                    if (sc.hasNextDouble()) {
                        calc.push(sc.nextDouble());
                    } else {
                        System.out.println("Must enter a numeric value!");
                    }
                } else if (input.equalsIgnoreCase("quit")) {
                    break;
                } else if (input.equalsIgnoreCase("clear")) {
                    calc.clear();
                } else if (calc.size() >= 1 && input.equalsIgnoreCase("sqrt")) {
                    calc.sqrt();
                } else if (calc.size() >= 2) {
                    if (input.equalsIgnoreCase("add") || input.equals("+")) {
                        calc.add();
                    } else if (input.equalsIgnoreCase("sub") || input.equals("-")) {
                        calc.subtract();
                    } else if (input.equalsIgnoreCase("mult") || input.equals("*")) {
                        calc.multiply();
                    } else if (input.equalsIgnoreCase("div") || input.equals("/")) {
                        calc.divide();
                    } else if (input.equalsIgnoreCase("pow") || input.equals("^")) {
                        calc.power();
                    } else {
                        System.out.println("Unknown command");
                    }
                } else {
                    System.out.println("Not enough arguments on the stack! (Try using push first)");
                }
            }
            sc.nextLine();

            for (double d : calc.getValues()) {
                System.out.println(d);
            }
            System.out.println();
        }

        System.out.println();
        System.out.println("Thanks for using the Stack Calculator.");
    }
}
