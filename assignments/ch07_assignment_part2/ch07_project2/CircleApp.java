import java.util.Scanner;

/**
 * @author Jeremy Wood
 */
public class CircleApp {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Welcome to the Circle Tester");

        do {
            System.out.println();

            double radius = Validator.getDouble(sc, "Enter radius:  ", 0, Double.MAX_VALUE);
            Circle circle = new Circle(radius);

            System.out.println("Circumference: " + circle.getFormattedCircumference());
            System.out.println("Area:          " + circle.getFormattedArea());
            System.out.println();
        } while (checkContinue(sc, "Continue? (y/n): ", "y"));

        System.out.println();
        System.out.println("Goodbye. You created " + Circle.getObjectCount() + " Circle objects(s)");
    }

    private static boolean checkContinue(Scanner sc, String prompt, String continueString) {
        System.out.print(prompt);
        try {
            return sc.next().equalsIgnoreCase(continueString);
        } finally {
            sc.nextLine();
        }
    }
}
