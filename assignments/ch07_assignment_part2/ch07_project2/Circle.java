import java.text.DecimalFormat;

/**
 * @author Jeremy Wood
 */
public class Circle {

    private static int circleCount = 0;

    private static final DecimalFormat MAX_2_DECIMALS = new DecimalFormat("#.##");

    private final double radius;

    public Circle(double radius) {
        this.radius = radius;
        circleCount++;
    }

    public double getCircumference() {
        return 2 * Math.PI * radius;
    }

    public double getArea() {
        return Math.PI * radius * radius;
    }

    public String getFormattedCircumference() {
        return formatNumber(getCircumference());
    }

    public String getFormattedArea() {
        return formatNumber(getArea());
    }

    private String formatNumber(double x) {
        return MAX_2_DECIMALS.format(x);
    }

    public static int getObjectCount() {
        return circleCount;
    }
}
