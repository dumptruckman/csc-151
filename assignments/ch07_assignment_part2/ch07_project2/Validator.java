import java.util.Scanner;

/**
 * @author Jeremy Wood
 */
public class Validator {

    public static double getDouble(Scanner sc, String prompt) {
        while (true) {
            System.out.print(prompt);
            if (sc.hasNextDouble()) {
                try {
                    return sc.nextDouble();
                } finally {
                    sc.nextLine();
                }
            } else {
                System.out.println("Error! Invalid decimal value. Try again.");
            }
            sc.nextLine();
        }
    }

    public static double getDouble(Scanner sc, String prompt, double min, double max) {
        while (true) {
            double d = getDouble(sc, prompt);
            if (d <= min) {
                System.out.println("Error! Number must be greater than " + min + ".");
            } else if (d >= max) {
                System.out.println("Error! Number must be less than " + max + ".");
            } else {
                return d;
            }
        }
    }
}
