import java.util.Scanner;

public class InvoiceApp
{
    public static void main(String[] args)
    {
        // display a welcome message
        System.out.println("Welcome to the Invoice Total Calculator");
        System.out.println();  // print a blank line

        Scanner sc = new Scanner(System.in);
        String choice = "y";
        while(choice.equalsIgnoreCase("y"))
        {
            // get user entries
            CustomerType customerType = CustomerType.getValidCustomerType(sc);
            double subtotal = Validator.getDouble(sc, "Enter subtotal:            ", 0, 10000);
            Invoice invoice = new Invoice(customerType, subtotal);

            // format and display the results
            String message = "Subtotal:         " + invoice.getFormattedSubtotal() + "\n"
                           + "Customer type:    " + invoice.getCustomerTypeName() + "\n"
                           + "Discount percent: " + invoice.getFormattedDiscountPercent() + "\n"
                           + "Discount amount:  " + invoice.getFormattedDiscountAmount() + "\n"
                           + "Total:            " + invoice.getFormattedTotal() + "\n";
            System.out.println();
            System.out.println(message);

            System.out.print("Continue? (y/n): ");
            choice = sc.next();
            System.out.println();
        }
    }
}