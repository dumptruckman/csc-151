import java.text.NumberFormat;

/**
 * @author Jeremy Wood
 */
public class Invoice {

    private CustomerType customerType;
    private double subtotal;

    public Invoice(CustomerType customerType, double subtotal) {
        this.customerType = customerType;
        this.subtotal = subtotal;
    }

    public String getCustomerTypeName() {
        return customerType.getName();
    }

    public double getSubtotal() {
        return subtotal;
    }

    public double getDiscountPercent() {
        switch (customerType) {
            case R:
                if (subtotal >= 500)
                    return .2;
                else if (subtotal >= 250 && subtotal < 500)
                    return .15;
                else if (subtotal >= 100 && subtotal < 250)
                    return .1;
                else
                    return .0;
            case C:
                return .2;
            default:
                return .05;
        }
    }

    public double getDiscountAmount() {
        return getDiscountPercent() * getSubtotal();
    }

    public double getTotal() {
        return getSubtotal() - getDiscountAmount();
    }

    public String getFormattedSubtotal() {
        return NumberFormat.getCurrencyInstance().format(getSubtotal());
    }

    public String getFormattedDiscountPercent() {
        return NumberFormat.getPercentInstance().format(getDiscountPercent());
    }

    public String getFormattedDiscountAmount() {
        return NumberFormat.getCurrencyInstance().format(getDiscountAmount());
    }

    public String getFormattedTotal() {
        return NumberFormat.getCurrencyInstance().format(getTotal());
    }
}
