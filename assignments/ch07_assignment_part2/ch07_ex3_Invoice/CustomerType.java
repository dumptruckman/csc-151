import java.util.Scanner;

public enum CustomerType {
    R("Retail"), C("College");

    private String name;

    CustomerType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static CustomerType getValidCustomerType(Scanner reader) {
        while (true) {
            System.out.print("Enter customer type (r/c): ");
            switch (reader.next()) {
                case "r":
                case "R":
                    reader.nextLine();
                    return CustomerType.R;
                case "c":
                case "C":
                    reader.nextLine();
                    return CustomerType.C;
                default:
                    reader.nextLine();
                    System.out.println("Invalid customer type!");
            }
        }
    }
}
