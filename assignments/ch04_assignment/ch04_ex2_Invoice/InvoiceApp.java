import java.text.NumberFormat;
import java.util.Scanner;

/**
 * @author Jeremy Wood
 */
public class InvoiceApp
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        String choice = "y";

        while (!choice.equalsIgnoreCase("n"))
        {
            // get the input from the user
            System.out.print("Enter customer type (R/C/T): ");
            String customerType = sc.next();
            System.out.print("Enter subtotal:   ");
            double subtotal = sc.nextDouble();

            // get the discount percent
            double discountPercent = getDiscountPercent(customerType, subtotal);

            // calculate the discount amount and total
            double discountAmount = subtotal * discountPercent;
            double total = subtotal - discountAmount;

            // format and display the results
            NumberFormat currency = NumberFormat.getCurrencyInstance();
            NumberFormat percent = NumberFormat.getPercentInstance();
            System.out.println(
                "Discount percent: " + percent.format(discountPercent) + "\n" +
                "Discount amount:  " + currency.format(discountAmount) + "\n" +
                "Total:            " + currency.format(total) + "\n");

            // see if the user wants to continue
            System.out.print("Continue? (y/n): ");
            choice = sc.next();
            System.out.println();
        }
    }

    public static double getDiscountPercent(String customerType, double subtotal) {
        switch (customerType) {
            case "R":
                if (subtotal < 100)
                    return 0D;
                else if (subtotal >= 100 && subtotal < 250)
                    return  .1D;
                else if (subtotal >= 250 && subtotal < 500)
                    return .25D;
                else
                    return .3D;
            case "C":
                return .2D;
            case "T":
                if (subtotal < 500)
                    return .4;
                else
                    return .5;
            default:
                return 0D;
        }
    }
}