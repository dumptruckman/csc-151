import java.util.Scanner;

/**
 * @author Jeremy Wood
 */
public class PowersTable {

    private static final String ROW_FORMAT = "%-8s%-8s%-8s";

    public static void main(String[] args) {

        Scanner reader = new Scanner(System.in);

        System.out.println("Welcome to the Squares and Cubes table");

        do {
            System.out.println();
            System.out.print("Enter an integer: ");
            int targetValue = reader.nextInt();

            System.out.println();
            System.out.println(String.format(ROW_FORMAT + "\n" + ROW_FORMAT,
                    "Number", "Squared", "Cubed",
                    "======", "=======", "====="));

            for (int i = 1; i <= targetValue; i++) {
                System.out.println(String.format(ROW_FORMAT, i, i * i, i * i * i));
            }

            System.out.println();
            System.out.print("Continue? (y/n): ");
        } while (reader.next().equalsIgnoreCase("y"));
    }
}
