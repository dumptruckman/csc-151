import java.text.NumberFormat;
import java.util.Scanner;

/**
 * @author Jeremy Wood
 */
public class TestScoreApp {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        do {
            System.out.println();
            System.out.print("Enter the number of test scores to be entered: ");
            int numberOfScores = getNumberOfScores(reader);
            getScores(reader, numberOfScores);
            System.out.println();
            System.out.print("Enter more test scores? (y/n): ");
        } while(reader.next().equalsIgnoreCase("y"));
    }

    private static void getScores(Scanner reader, int numberOfScores) {
        int scoreTotal = 0;
        int scoreCount = 0;
        int testScore = 0;
        Integer minScore = null, maxScore = null;

        // get a series of test scores from the user
        do {
            scoreCount++;
            // get the input from the user
            System.out.print("Enter score " + scoreCount + ": ");
            testScore = getScore(reader);
            minScore = Math.min(minScore != null ? minScore : 100, testScore);
            maxScore = Math.max(maxScore != null ? minScore : 0, testScore);
            scoreTotal += testScore;
        } while (scoreCount < numberOfScores);

        // display the score count, score total, and average score
        double averageScore = (double) scoreTotal / scoreCount;
        NumberFormat oneDecimalFormat = NumberFormat.getNumberInstance();
        oneDecimalFormat.setMaximumFractionDigits(1);
        String message = "\n"
                + "Score count:   " + scoreCount + "\n"
                + "Score total:   " + scoreTotal + "\n"
                + "Average score: " + oneDecimalFormat.format(averageScore) + "\n"
                + "Minimum score: " + minScore + "\n"
                + "Maximum score: " + maxScore + "\n\n";
        System.out.printf(message);
    }

    private static int getNumberOfScores(Scanner reader) {
        while (true) {
            int num = getInteger(reader);
            if (num > 0) {
                return num;
            } else {
                System.out.print("Invalid input. Please enter a number greater than 0: ");
            }
        }
    }

    private static int getScore(Scanner reader) {
        while (true) {
            int num = getInteger(reader);
            if (num >= 0 && num <= 100) {
                return num;
            } else {
                System.out.print("Invalid input. Please enter a number from 0 to 100: ");
            }
        }
    }

    private static int getInteger(Scanner reader) {
        while (!reader.hasNextInt()) {
            reader.next();
            System.out.print("Invalid input. Please enter a whole number: ");
        }
        return reader.nextInt();
    }
}