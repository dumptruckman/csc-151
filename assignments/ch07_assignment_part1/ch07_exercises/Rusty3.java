import java.text.NumberFormat;

/**
 * @author Jeremy Wood
 */
public class Rusty3 {

    public static void main(String[] args) {
        Car car1 = new Car();
        Car car2 = new Car("JJJ1123341", "Nissan", "Maxima", 1996, 2000, 200, "Fair", "Black");
        Car car3 = new Car("5533V11551", "Honda", "Fit", 2008, 4500, 150, "Good", "Silver");

        car1.setVIN("1123455123");
        car1.setMake("Nissan");
        car1.setModel("Versa");
        car1.setModelYear(2011);
        car1.setDealerCost(8000);
        car1.setCleaningCost(50);
        car1.setDescription("Blue");

        Car car4 = car1.copy();
        car4.setModelYear(2014);
        car4.setDealerCost(14000);

        printCarDetails(car1);
        printCarDetails(car2);
        printCarDetails(car3);
        printCarDetails(car4);
    }

    private static void printCarDetails(Car car) {
        System.out.println(car.getModelYear() + " " + car.getMake() + " " + car.getModel()
                + " (" + NumberFormat.getCurrencyInstance().format(car.getRetailPrice()) + ")");
    }
}
