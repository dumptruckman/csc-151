/**
 * @author Jeremy Wood
 */
public class Rusty1 {

    public static void main(String[] args) {

        System.out.println(Dealership.getCompanyBanner());
        System.out.println("We've been operating now for " + Dealership.getYearsInBusiness()
                + " years! To talk to one of our sales people, send an email to " + Dealership.COMPANY_EMAIL + ".");
    }
}
