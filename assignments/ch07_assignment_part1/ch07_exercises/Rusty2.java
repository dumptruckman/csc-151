/**
 * @author Jeremy Wood
 */
import java.text.NumberFormat;
import java.util.Scanner;

public class Rusty2 {

    public static void main(String[] args) {
        System.out.println("Welcome to " + Dealership.COMPANY_NAME + " car cost calculator.");

        Scanner reader = new Scanner(System.in);

        do {
            System.out.println();
            System.out.print("Please input the dealer cost: ");
            double dealerCost = getDollarAmount(reader);
            System.out.print("Please input the cleaning cost: ");
            double cleaningCost = getDollarAmount(reader);
            System.out.println();
            System.out.println("Calculated retail cost: "
                    + NumberFormat.getCurrencyInstance().format(Dealership.getRetailPrice(dealerCost, cleaningCost)));
            System.out.println();

            System.out.print("Check another? (y/n): ");
        } while(reader.next().equalsIgnoreCase("y"));
    }

    private static double getDollarAmount(Scanner reader) {
        while (!reader.hasNextDouble()) {
            reader.next();
            System.out.print("That is not a valid entry. Please enter a dollar value: ");
        }
        return reader.nextDouble();
    }
}
