/**
 * @author Jeremy Wood
 */
public class GameScene {

    public static void main(String[] args) {
        GameCharacter char1 = new GameCharacter("Joe", "Elf", 8, 420);
        GameCharacter char2 = new GameCharacter("Mary", "Wizard");
        char2.setCharHealth(5);

        System.out.println(String.format("%s the %s is standing at a crossroads. "
                + "Suddenly there is a flash of light and %s the %s appears!",
                char1.getCharName(), char1.getCharType(),
                char2.getCharName(), char2.getCharType()));
        printCharacterStatus(char1);
        printCharacterStatus(char2);
    }

    private static void printCharacterStatus(GameCharacter c) {
        System.out.println(String.format("%s's health rating is %s", c.getCharName(), c.getCharHealth()));
        System.out.println(String.format("%s's current score is %s", c.getCharName(), c.getCharScore()));
    }
}
