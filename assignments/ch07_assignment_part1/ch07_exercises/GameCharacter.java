/**
 * @author Jeremy Wood
 */
public class GameCharacter {

    private String charName, charType;
    private int charHealth, charScore;

    public GameCharacter(String charName, String charType, int charHealth, int charScore) {
        this.charName = charName;
        this.charType = charType;
        this.charHealth = charHealth;
        this.charScore = charScore;
    }

    public GameCharacter(String charName, String charType) {
        this(charName, charType, 0, 0);
    }

    public String getCharName() {
        return charName;
    }

    public void setCharName(String charName) {
        this.charName = charName;
    }

    public String getCharType() {
        return charType;
    }

    public void setCharType(String charType) {
        this.charType = charType;
    }

    public int getCharHealth() {
        return charHealth;
    }

    public void setCharHealth(int charHealth) {
        this.charHealth = charHealth;
    }

    public int getCharScore() {
        return charScore;
    }

    public void setCharScore(int charScore) {
        this.charScore = charScore;
    }
}
