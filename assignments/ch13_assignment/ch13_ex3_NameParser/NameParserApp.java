import java.util.Scanner;

/**
 * @author Jeremy
 */
public class NameParserApp {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Welcome to the name parser.\n");
        System.out.print("Enter a name: ");
        String name = sc.nextLine();

        System.out.println();

        String[] names = name.split("\\s");
        if (names.length < 2 || names.length > 3) {
            System.out.println("You name must consist of either 2 or 3 words only.");
        } else {
            System.out.println("First name: " + names[0]);
            int i = 1;
            if (names.length > 2) {
                System.out.println("Middle name: " + names[i]);
                i++;
            }
            System.out.println("Last name: " + names[i]);
        }
    }
}