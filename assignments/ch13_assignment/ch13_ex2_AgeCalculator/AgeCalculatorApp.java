import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/**
 * @author Jeremy
 */
public class AgeCalculatorApp {

    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("MMM d, uuuu");

    public static void main(String[] args) {
        //Get the user's birthdate
        System.out.println("Welcome to the age calculator.");
        Scanner sc = new Scanner(System.in);
        LocalDate currentDate = LocalDate.now(); // LocalDate because it is the Java 8 replacement for the antiquated Date and Calendar classes
        int birthMonth = Validator.getInt(sc, "Enter the month you were born (1 to 12): ",
                0, 13);
        int birthDay = Validator.getInt(sc, "Enter the day of the month you were born: ",
                0, 32);
        int birthYear = Validator.getInt(sc, "Enter the year you were born (four digits): ",
                currentDate.getYear() - 111, currentDate.getYear() + 1);

        System.out.println();

        LocalDate birthDate = LocalDate.of(birthYear, birthMonth, birthDay);
        Period period = Period.between(birthDate, currentDate);
        System.out.println("Current Date: " + currentDate.format(DATE_FORMAT));
        System.out.println("Birth Date: " + birthDate.format(DATE_FORMAT));
        System.out.println();
        int years = period.getYears();
        period = period.minusYears(years);
        int monthsOfYear = period.getMonths();
        period = period.minusMonths(monthsOfYear);
        int daysOfMonth = period.getDays();
        System.out.println("Age: " + years + " year" + (years != 1 ? "s" : "") + ", "
                + monthsOfYear + " month" + (monthsOfYear != 1 ? "s" : "") + " and "
                + daysOfMonth + " day" + (daysOfMonth != 1 ? "s" : ""));
    }
}
