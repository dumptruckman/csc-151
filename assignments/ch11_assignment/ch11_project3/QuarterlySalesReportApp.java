import java.text.NumberFormat;
import java.util.Arrays;
import java.util.function.Consumer;

/**
 * @author Jeremy Wood
 */
public class QuarterlySalesReportApp {

    public static void main(String[] args) {
        double sales[][] = {
                {1540D, 2010D, 2450D, 1845D},
                {1130D, 1168D, 1847D, 1491D},
                {1580D, 2305D, 2710D, 1284D},
                {1105D, 4102D, 2391D, 1576D}
        };

        System.out.println("Welcome to the Sales Report Application.");
        System.out.println();

        System.out.println("Region\tQ1\t\t\tQ2\t\t\tQ3\t\t\tQ4");
        for (int i = 0; i < sales.length; i++) {
            System.out.print(i + 1 + "\t\t");
            for (int j = 0; j < sales[i].length; j++) {
                if (j != 0) {
                    System.out.print("\t");
                }
                System.out.print(NumberFormat.getCurrencyInstance().format(sales[i][j]));
            }
            System.out.println();
        }
        System.out.println();

        System.out.println("Sales by region:");
        for (int i = 0; i < sales.length; i++) {
            System.out.print("Region " + (i + 1) + ": ");
            double total = 0;
            for (int j = 0; j < sales[i].length; j++) {
                total += sales[i][j];
            }
            System.out.println(NumberFormat.getCurrencyInstance().format(total));
        }
        System.out.println();

        System.out.println("Sales by quarter:");
        for (int j = 0, col = 0; col < sales.length && j < sales[col].length; j++, col++) {
            System.out.print("Q" + (j + 1) + ": ");
            double total = 0;
            for (int i = 0; i < sales.length; i++) {
                total += sales[i][j];
            }
            System.out.println(NumberFormat.getCurrencyInstance().format(total));
        }
        System.out.println();

        double total = 0;
        for (int i = 0; i < sales.length; i++) {
            for (int j = 0; j < sales[i].length; j++) {
                total += sales[i][j];
            }
        }
        System.out.println("Total annual sales, all regions: " + NumberFormat.getCurrencyInstance().format(total));
    }
}
