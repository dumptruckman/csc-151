import java.util.Arrays;

/**
 * @author Jeremy Wood
 */
public class ArrayTestApp {

    public static void main(String[] args) {
        double[] values = new double[99];

        for (int i = 0; i < values.length; i++) {
            values[i] = Math.random() * 100;
        }

        double total = 0;
        for (double value : values) {
            total += value;
        }
        System.out.println(total / values.length);
        System.out.println();

        Arrays.sort(values);
        System.out.println(values[49]);
        System.out.println();

        for (int i = 8; i < values.length; i += 9) {
            System.out.println(values[i]);
        }
    }
}
