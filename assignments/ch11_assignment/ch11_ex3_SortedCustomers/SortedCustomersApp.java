import java.util.Arrays;

/**
 * @author Jeremy Wood
 */
public class SortedCustomersApp {
    public static void main(String[] args) {
        Customer[] customers = {
                new Customer("popadoc@aol.com", "Doc", "Jones"),
                new Customer("garyyyr@gmail.com", "Gary", "Wilson"),
                new Customer("hd4101@zy.zx.net", "Kent", "Masters")
        };
        Arrays.sort(customers);

        for (Customer customer : customers) {
            System.out.println(customer.getLastName() + ", " + customer.getFirstName() + " (" + customer.getEmail() + ")");
        }
    }
}
