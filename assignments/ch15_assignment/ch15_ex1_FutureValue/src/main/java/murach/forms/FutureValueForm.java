package murach.forms;

import murach.business.FinancialCalculations;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.NumberFormat;

/**
 * @author Jeremy Wood
 */
public class FutureValueForm extends JFrame {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new FutureValueForm().setVisible(true));
    }

    private JLabel lblMonthlyPayment, lblYearlyInterestRate, lblNumberOfYears, lblFutureValue;
    private JTextField txtMonthlyPayment, txtYearlyInterestRate, txtNumberOfYears, txtFutureValue;
    private JButton btnCalculate, btnExit;

    private SwingValidator validator = new SwingValidator();

    public FutureValueForm() {
        setTitle("Future Value Calculator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(450, 250);
        setLocationRelativeTo(this);
        getContentPane().add(initComponents());
        pack();
    }

    private JPanel initComponents() {
        JPanel panel = new JPanel(new MigLayout());

        lblMonthlyPayment = new JLabel("Monthly Payment");
        lblYearlyInterestRate = new JLabel("Yearly Interest Rate");
        lblNumberOfYears = new JLabel("Number of Years");
        lblFutureValue = new JLabel("Future Value");

        NumberFilter numberFilter = new NumberFilter();
        txtMonthlyPayment = new FocusedTextField(10);
        ((AbstractDocument) txtMonthlyPayment.getDocument()).setDocumentFilter(numberFilter);
        txtYearlyInterestRate = new FocusedTextField(10);
        ((AbstractDocument) txtYearlyInterestRate.getDocument()).setDocumentFilter(numberFilter);
        txtNumberOfYears = new FocusedTextField(10);
        ((AbstractDocument) txtNumberOfYears.getDocument()).setDocumentFilter(new IntegerFilter());
        txtFutureValue = new JTextField(10);
        txtFutureValue.setEditable(false);

        btnCalculate = new JButton("Calculate");
        btnCalculate.addActionListener(e -> {
            if (validator.isPresent(txtMonthlyPayment, "Monthly Payment")
                    && validator.isDouble(txtMonthlyPayment, "Monthly Payment")
                    && validator.isPresent(txtYearlyInterestRate, "Yearly Interest Rate")
                    && validator.isDouble(txtYearlyInterestRate, "Yearly Interest Rate")
                    && validator.isPresent(txtNumberOfYears, "Number of Years")
                    && validator.isInteger(txtNumberOfYears, "Number of Years")) {
                double monthlyPayment = Double.valueOf(txtMonthlyPayment.getText());
                double yearlyInterestRate = Double.valueOf(txtYearlyInterestRate.getText());
                int numberOfYears = Integer.valueOf(txtNumberOfYears.getText());
                txtFutureValue.setText(NumberFormat.getCurrencyInstance().format(FinancialCalculations.calculateFutureValue(monthlyPayment, yearlyInterestRate, numberOfYears)));
            }
        });

        btnExit = new JButton("Exit");
        btnExit.addActionListener(e -> FutureValueForm.this.dispose());

        panel.add(lblMonthlyPayment, "cell 0 0");
        panel.add(txtMonthlyPayment, "cell 1 0");
        panel.add(lblYearlyInterestRate, "cell 0 1");
        panel.add(txtYearlyInterestRate, "cell 1 1");
        panel.add(lblNumberOfYears, "cell 0 2");
        panel.add(txtNumberOfYears, "cell 1 2");
        panel.add(lblFutureValue, "cell 0 3");
        panel.add(txtFutureValue, "cell 1 3, wrap");

        JPanel buttonPanel = new JPanel(new MigLayout());
        panel.add(buttonPanel, "span 2, right");
        buttonPanel.add(btnCalculate, "right");
        buttonPanel.add(btnExit, "right");

        return panel;
    }

    private static class NumberFilter extends DocumentFilter {

        @Override
        public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
            fb.insertString(offset, string.replaceAll("[^0-9.]", ""), attr);
        }

        @Override
        public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
            fb.replace(offset, length, text.replaceAll("[^0-9.]", ""), attrs);
        }
    }

    private static class IntegerFilter extends DocumentFilter {

        @Override
        public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
            fb.insertString(offset, string.replaceAll("[^0-9]", ""), attr);
        }

        @Override
        public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
            fb.replace(offset, length, text.replaceAll("[^0-9]", ""), attrs);
        }
    }

    static class FocusedTextField extends JTextField {

        {
            addFocusListener(new FocusListener() {
                @Override
                public void focusGained(FocusEvent e) {
                    FocusedTextField.this.select(0, getText().length());
                }

                @Override
                public void focusLost(FocusEvent e) {
                    FocusedTextField.this.select(0, 0);
                }
            });
        }

        public FocusedTextField(int columns) {
            super(columns);
        }
    }
}
