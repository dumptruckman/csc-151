package wood.forms;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DecimalFormat;

/**
 * @author Jeremy Wood
 */
public class HypotenuseCalculatorForm extends JFrame {

    private static final DecimalFormat MAX_3_DECIMALS = new DecimalFormat("#.###");

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new HypotenuseCalculatorForm().setVisible(true));
    }

    private JLabel lblSideA, lblSideB, lblSideC;
    private JTextField txtSideA, txtSideB, txtSideC;
    private JButton btnCalculate, btnExit;

    private SwingValidator validator = new SwingValidator();

    public HypotenuseCalculatorForm() {
        setTitle("Right Triangles");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(450, 250);
        setLocationRelativeTo(this);
        getContentPane().add(initComponents());
        pack();
    }

    private JPanel initComponents() {
        JPanel panel = new JPanel(new MigLayout());

        lblSideA = new JLabel("Side A");
        lblSideB = new JLabel("Side B");
        lblSideC = new JLabel("Side C");

        NumberFilter numberFilter = new NumberFilter();
        txtSideA = new FocusedTextField(15);
        ((AbstractDocument) txtSideA.getDocument()).setDocumentFilter(numberFilter);
        txtSideB = new FocusedTextField(15);
        ((AbstractDocument) txtSideB.getDocument()).setDocumentFilter(numberFilter);
        txtSideC = new JTextField(15);
        txtSideC.setEditable(false);

        btnCalculate = new JButton("Calculate");
        btnCalculate.addActionListener(e -> {
            if (validator.isPresent(txtSideA, "Side A")
                    && validator.isDouble(txtSideA, "Side A")
                    && validator.isPresent(txtSideB, "Side B")
                    && validator.isDouble(txtSideB, "Side B")) {
                double sideA = Double.valueOf(txtSideA.getText());
                double sideB = Double.valueOf(txtSideB.getText());
                double sideC = Math.sqrt(sideA * sideA + sideB * sideB);
                txtSideC.setText(MAX_3_DECIMALS.format(sideC));
            }
        });

        btnExit = new JButton("Exit");
        btnExit.addActionListener(e -> HypotenuseCalculatorForm.this.dispose());

        panel.add(lblSideA, "cell 0 0");
        panel.add(txtSideA, "cell 1 0");
        panel.add(lblSideB, "cell 0 1");
        panel.add(txtSideB, "cell 1 1");
        panel.add(lblSideC, "cell 0 2");
        panel.add(txtSideC, "cell 1 2, wrap");

        JPanel buttonPanel = new JPanel(new MigLayout());
        panel.add(buttonPanel, "span 2, right");
        buttonPanel.add(btnCalculate, "right");
        buttonPanel.add(btnExit, "right");

        return panel;
    }

    private static class NumberFilter extends DocumentFilter {

        @Override
        public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
            fb.insertString(offset, string.replaceAll("[^0-9.]", ""), attr);
        }

        @Override
        public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
            fb.replace(offset, length, text.replaceAll("[^0-9.]", ""), attrs);
        }
    }

    static class FocusedTextField extends JTextField {

        {
            addFocusListener(new FocusListener() {
                @Override
                public void focusGained(FocusEvent e) {
                    FocusedTextField.this.select(0, getText().length());
                }

                @Override
                public void focusLost(FocusEvent e) {
                    FocusedTextField.this.select(0, 0);
                }
            });
        }

        public FocusedTextField(int columns) {
            super(columns);
        }
    }
}
