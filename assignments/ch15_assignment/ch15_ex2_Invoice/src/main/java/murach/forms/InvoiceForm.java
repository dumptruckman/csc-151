package murach.forms;

import murach.business.InvoiceCalculations;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.NumberFormat;

/**
 * @author Jeremy Wood
 */
public class InvoiceForm extends JFrame {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new InvoiceForm().setVisible(true));
    }

    private JLabel lblCustomerType, lblSubtotal, lblDiscountPercent, lblDiscountAmount, lblTotal;
    private JTextField txtCustomerType, txtSubtotal, txtDiscountPercent, txtDiscountAmount, txtTotal;
    private JButton btnCalculate, btnExit;

    private SwingValidator validator = new SwingValidator();

    public InvoiceForm() {
        setTitle("Invoice Total Calculator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(450, 250);
        setLocationRelativeTo(this);
        getContentPane().add(initComponents());
        pack();
    }

    private JPanel initComponents() {
        JPanel panel = new JPanel(new MigLayout());

        lblCustomerType = new JLabel("Customer Type");
        lblSubtotal = new JLabel("Subtotal");
        lblDiscountPercent = new JLabel("Discount Percent");
        lblDiscountAmount = new JLabel("Discount Amount");
        lblTotal = new JLabel("Total");

        txtCustomerType = new FocusedTextField(2);
        txtSubtotal = new FocusedTextField(10);
        ((AbstractDocument) txtSubtotal.getDocument()).setDocumentFilter(new NumberFilter());
        txtDiscountPercent = new JTextField(10);
        txtDiscountPercent.setEditable(false);
        txtDiscountAmount = new JTextField(10);
        txtDiscountAmount.setEditable(false);
        txtTotal = new JTextField(10);
        txtTotal.setEditable(false);

        btnCalculate = new JButton("Calculate");
        btnCalculate.addActionListener(e -> {
            if (validator.isPresent(txtCustomerType, "Customer Type")
                    && validator.isPresent(txtSubtotal, "Subtotal")
                    && validator.isDouble(txtSubtotal, "Subtotal")) {
                String customerType = txtCustomerType.getText();
                double subtotal = Double.valueOf(txtSubtotal.getText());
                double discountPercent = InvoiceCalculations.calculateDiscountPct(customerType, subtotal);
                double discountAmount = subtotal * discountPercent;
                double total = subtotal - discountAmount;
                txtDiscountPercent.setText(NumberFormat.getPercentInstance().format(discountPercent));
                txtDiscountAmount.setText(NumberFormat.getCurrencyInstance().format(discountAmount));
                txtTotal.setText(NumberFormat.getCurrencyInstance().format(total));
            }
        });

        btnExit = new JButton("Exit");
        btnExit.addActionListener(e -> InvoiceForm.this.dispose());

        panel.add(lblCustomerType, "cell 0 0");
        panel.add(txtCustomerType, "cell 1 0");
        panel.add(lblSubtotal, "cell 0 1");
        panel.add(txtSubtotal, "cell 1 1");
        panel.add(lblDiscountPercent, "cell 0 2");
        panel.add(txtDiscountPercent, "cell 1 2");
        panel.add(lblDiscountAmount, "cell 0 3");
        panel.add(txtDiscountAmount, "cell 1 3");
        panel.add(lblTotal, "cell 0 4");
        panel.add(txtTotal, "cell 1 4, wrap");

        JPanel buttonPanel = new JPanel(new MigLayout());
        panel.add(buttonPanel, "span 2, right");
        buttonPanel.add(btnCalculate, "right");
        buttonPanel.add(btnExit, "right");

        return panel;
    }

    private static class NumberFilter extends DocumentFilter {

        @Override
        public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
            fb.insertString(offset, string.replaceAll("[^0-9.]", ""), attr);
        }

        @Override
        public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
            fb.replace(offset, length, text.replaceAll("[^0-9.]", ""), attrs);
        }
    }

    static class FocusedTextField extends JTextField {

        {
            addFocusListener(new FocusListener() {
                @Override
                public void focusGained(FocusEvent e) {
                    FocusedTextField.this.select(0, getText().length());
                }

                @Override
                public void focusLost(FocusEvent e) {
                    FocusedTextField.this.select(0, 0);
                }
            });
        }

        public FocusedTextField(int columns) {
            super(columns);
        }
    }
}
