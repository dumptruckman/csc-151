import java.text.NumberFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author Jeremy Wood
 */
public class InvoiceApp
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        String choice = "y";

        while (!choice.equalsIgnoreCase("n"))
        {
            // get the input from the user
            CustomerType customerType = getValidCustomerType(sc);
            sc.nextLine();
            double subtotal;
            try {
                subtotal = getValidSubtotal(sc);
            } catch (InputMismatchException e) {
                System.out.println("Invalid subtotal! Start over.");
                continue;
            }
            sc.nextLine();

            // get the discount percent
            double discountPercent = 0;
            if (customerType == CustomerType.R)
            {
                if (subtotal < 100)
                    discountPercent = 0;
                else if (subtotal >= 100 && subtotal < 250)
                    discountPercent = .1;
                else if (subtotal >= 250)
                    discountPercent = .2;
            }
            else if (customerType == CustomerType.C)
            {
                if (subtotal < 250)
                    discountPercent = .2;
                else
                    discountPercent = .3;
            }
            else
            {
                discountPercent = .1;
            }
            
            // calculate the discount amount and total
            double discountAmount = subtotal * discountPercent;
            double total = subtotal - discountAmount;
            
            // format and display the results
            NumberFormat currency = NumberFormat.getCurrencyInstance();
            NumberFormat percent = NumberFormat.getPercentInstance();
            System.out.println(
                    "Discount percent: " + percent.format(discountPercent) + "\n" +
                    "Discount amount:  " + currency.format(discountAmount) + "\n" +
                    "Total:            " + currency.format(total) + "\n");
            
            // see if the user wants to continue
            System.out.print("Continue? (y/n): ");
            choice = sc.next();
            sc.nextLine();
            System.out.println();
        }
    }

    private static CustomerType getValidCustomerType(Scanner reader) {
        while (true) {
            System.out.print("Enter customer type (r/c): ");
            switch (reader.next()) {
                case "r":
                case "R":
                    return CustomerType.R;
                case "c":
                case "C":
                    return CustomerType.C;
                default:
                    reader.nextLine();
                    System.out.println("Invalid customer type!");
            }
        }
    }

    private static double getValidSubtotal(Scanner reader) {
        while (true) {
            System.out.print("Enter subtotal:   ");
            if (!reader.hasNextDouble()) {
                reader.nextLine();
                System.out.println("Invalid subtotal! Entry must be a decimal number.");
            } else {
                double input = reader.nextDouble();
                if (input > 0 && input < 10000) {
                    return input;
                } else {
                    System.out.println("Invalid subtotal! Entry must be between 0 and 10000.");
                    reader.nextLine();
                }
            }
        }
    }

    private enum CustomerType {
        R, C
    }
}